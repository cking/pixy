package pixkin

import (
	"bytes"
	"math/rand"
	"path/filepath"
	"sync"

	"github.com/cking/pixiv"

	"github.com/robfig/cron"
	"github.com/rs/zerolog"
	"gitlab.com/cking/pixy/config"
	"gitlab.com/cking/pixy/internal/mastodon"
	"gitlab.com/cking/pixy/internal/telegram"
	"gitlab.com/cking/pixy/services"
	"go.uber.org/multierr"
	"golang.org/x/xerrors"
)

// Pixkin is the pixy logic
type Pixkin struct {
	l     *zerolog.Logger
	c     *config.Configuration
	wg    *sync.WaitGroup
	pixiv *pixiv.Session
	cron  *cron.Cron

	connectors []Connector
}

// Configure fetches objects from the container and configures application specific logic
func (pk *Pixkin) Configure(c *services.Container) error {
	var err error

	pk.wg = new(sync.WaitGroup)

	if pk.l, err = c.GetLogger(); err != nil {
		return xerrors.Errorf("failed to fetch logger: %w", err)
	}

	if pk.c, err = c.GetConfig(); err != nil {
		return xerrors.Errorf("failed to fetch config: %w", err)
	}

	if pk.pixiv, err = c.GetPixiv(); err != nil {
		return xerrors.Errorf("failed to fetch config: %w", err)
	}

	if pk.c.Telegram.Enabled {
		t := new(telegram.Telegram)
		pk.connectors = append(pk.connectors, t)
		if err := t.Configure(c); err != nil {
			return xerrors.Errorf("failed to initialize telegram adapter: %w", err)
		}
	}

	if pk.c.Mastodon.Enabled {
		m := new(mastodon.Mastodon)
		pk.connectors = append(pk.connectors, m)
		if err := m.Configure(c); err != nil {
			return xerrors.Errorf("failed to initialize mastodon adapter: %w", err)
		}
	}

	pk.cron = cron.New()
	pk.cron.AddFunc("@every 20m", pk.pixySchedule)

	return nil
}

// Run launches this pixkin
func (pk *Pixkin) Run(c *services.Container) error {
	pk.wg.Add(len(pk.connectors))

	var merr error
	var m sync.Mutex
	for _, cn := range pk.connectors {
		go func(cn Connector) {
			err := cn.Run(c)
			if err != nil {
				m.Lock()
				multierr.Append(merr, err)
				m.Unlock()
			}
			pk.wg.Done()
		}(cn)
	}

	pk.pixySchedule()
	pk.cron.Start()

	pk.wg.Wait()
	return merr
}

func (pk *Pixkin) pixySchedule() {
	pk.l.Debug().Msg("image schedule")
	illusts, err := pk.pixiv.IllustRecommended(0)
	is := illusts.Illusts
	if err != nil {
		pk.l.Error().Err(err).Msg("failed to fetch pixiv recommendations")
		return
	}

	pk.l.Debug().Int("results", len(is)).Msg("found images!")
	if len(is) == 0 {
		return
	}

	n := rand.Intn(len(is))
	pk.l.Debug().Int("result", n).Msgf("using %v", is[n].ID)
	rc, err := pk.pixiv.Download(&is[n], pixiv.SizeOriginal, 0)
	if err != nil {
		pk.l.Error().Err(err).Msg("failed to fetch pixiv page")
		return
	}
	defer rc.Close()

	b := &bytes.Buffer{}
	_, err = b.ReadFrom(rc)
	if err != nil {
		pk.l.Error().Err(err).Msg("failed to fetch download image")
		return
	}

	fname := pk.pixiv.DownloadLink(&is[n], pixiv.SizeMedium, 0)
	fname = filepath.Base(fname)

	for _, c := range pk.connectors {
		c.Post(fname, b, &is[n])
	}

	pk.l.Debug().Msg("finished image schedule")
	// favorizing disabled, was bugged anyway
	// if pk.c.Pixiv.FavChance > rand.Float64()*100 {
	// 	pk.l.Info().Str("illust", pixiv.Page(is[n])).Msg("favorite chance!")
	// 	pk.pixiv.V2.IllustBookmark.Add(context.Background(), uint64(is[n].ID), "public")
	// }
}
