package pixkin

import (
	"bytes"

	"github.com/cking/pixiv"
	"gitlab.com/cking/pixy/services"
)

// Connector defines the connection to one of the clients
type Connector interface {
	services.Configurable
	services.Runnable

	Break()
	Post(filename string, buffer *bytes.Buffer, illust *pixiv.Illust)
}
