package mastodon

import (
	"bytes"
	"context"
	"fmt"

	"github.com/cking/pixiv"
	"github.com/mattn/go-mastodon"
	"github.com/rs/zerolog"
	"gitlab.com/cking/pixy/services"
	"golang.org/x/xerrors"
)

// Mastodon is the mastodon bot api connector
type Mastodon struct {
	bot   *mastodon.Client
	l     *zerolog.Logger
	pixiv *pixiv.Session

	breakLoop chan struct{}
}

// Configure fetches objects from the container and configures application specific logic
func (m *Mastodon) Configure(c *services.Container) error {
	var err error
	if m.bot, err = c.GetMastodon(); err != nil {
		return xerrors.Errorf("failed to fetch mastodon: %w", err)
	}

	if m.pixiv, err = c.GetPixiv(); err != nil {
		return xerrors.Errorf("failed to fetch pixiv: %w", err)
	}

	if m.l, err = c.GetLogger(); err != nil {
		return xerrors.Errorf("failed to fetch logger: %w", err)
	}
	l := m.l.With().Str("module", "mastodon").Logger()
	m.l = &l

	return nil
}

// Run launches this pixkin
func (m *Mastodon) Run(c *services.Container) error {
	acc, err := m.bot.GetAccountCurrentUser(context.Background())
	if err != nil {
		return err
	}
	m.l.Info().Str("username", acc.Username).Msg("authenticated!")

	<-m.breakLoop
	m.l.Info().Msg("Stopping!")
	return nil
}

// Break the main loop
func (m *Mastodon) Break() {
	close(m.breakLoop)
}

// Post the specified image to all subscribers
func (m *Mastodon) Post(filename string, buffer *bytes.Buffer, illust *pixiv.Illust) {
	m.l.Info().Msg("Starting post schedule")

	att, err := m.bot.UploadMediaFromReader(context.Background(), buffer)
	if err != nil {
		m.l.Error().Err(err).Msg("failed to upload image")
	}

	toot := new(mastodon.Toot)
	toot.Status = fmt.Sprintf("#cuteposting\n\nSauce: https://www.pixiv.net/member_illust.php?mode=medium&illust_id=%v", illust.ID)
	toot.MediaIDs = []mastodon.ID{att.ID}
	toot.Sensitive = true
	_, err = m.bot.PostStatus(context.Background(), toot)
	if err != nil {
		m.l.Error().Err(err).Msg("failed to post toot")
	}
}
