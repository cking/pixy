package telegram

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func (tg *Telegram) installCallbackHandlers() {
	tg.callbacks = map[string]msgHandler{
		"start":       tg.cStart,
		"subscribe":   tg.hSubscribe(true),
		"unsubscribe": tg.hUnsubscribe(true),
		"settings":    tg.cSettings,
		"quiet":       tg.hQuiet(true),
		"skip":        tg.hSkip(true),
		"search":      tg.hSearch(true),
	}
}

func (tg *Telegram) executeCallback(cb *tgbotapi.CallbackQuery) error {
	l := tg.l.With().
		Int64("chat_id", cb.Message.Chat.ID).
		Int("message_id", cb.Message.MessageID).
		Bool("callback", true).Logger()

	l.Debug().
		Str("data", cb.Data).
		Msg("callback query")

	// just a dirty hack
	cb.Message.From = cb.From

	handler, found := tg.callbacks[cb.Data]
	if !found {
		l.Warn().Msg("unkown callback found, skipping!")
		return nil
	}
	l.Debug().Msg("callback found, executing!")
	return handler(cb.Message)
}

func (tg *Telegram) cStart(m tbm) error {
	kb := &telegramDefaultKeyboard
	if tg.isSubscribed(m.Chat.ID) {
		kb = &telegramSubscribedKeyboard
	}

	return tg.edit(m, "Hello "+m.From.FirstName+`,

use the buttons below to have some fun.
	`, kb)
}

func (tg *Telegram) cSettings(m tbm) error {
	return tg.edit(m, "What do you want to change?", &telegramSettingsKeyboard)
}
