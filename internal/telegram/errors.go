package telegram

import "errors"

var (
	errUnparseable = errors.New("unparseable")
)
