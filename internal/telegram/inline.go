package telegram

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/cking/pixiv"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/minio/minio-go"
)

func (tg *Telegram) executeInline(iq *tgbotapi.InlineQuery) error {
	ic := tgbotapi.InlineConfig{
		InlineQueryID: iq.ID,
		IsPersonal:    false,
		CacheTime:     int((time.Hour * 24).Seconds()),
	}

	tg.l.Debug().Str("terms", iq.Query).Msg("starting search...")
	illusts, err := tg.pixiv.IllustSearch(iq.Query, pixiv.SearchPartialTags, 0)
	is := illusts.Illusts
	if err != nil {
		return err
	}
	tg.l.Debug().Str("terms", iq.Query).Int("results", len(is)).Msg("found images!")

	if len(is) == 0 {
		return nil
	}

	articles := make([]interface{}, len(is))
	wg := new(sync.WaitGroup)
	wg.Add(len(is))
	for i, illust := range is {
		article := tgbotapi.NewInlineQueryResultArticle(strconv.Itoa(illust.ID), illust.Title, fmt.Sprintf("https://www.pixiv.net/member_illust.php?mode=medium&illust_id=%v", illust))
		article.URL = fmt.Sprintf("https://www.pixiv.net/member_illust.php?mode=medium&illust_id=%v", illust)
		article.ThumbURL = fmt.Sprintf("https://zerotsu.z0ne.moe/pixiv/%v.jpg", illust.ID)

		go func() {
			tg.inlinePreview(&illust)
			wg.Done()
		}()

		articles[i] = article
	}
	ic.Results = articles

	wg.Wait()
	_, err = tg.bot.AnswerInlineQuery(ic)

	return err
}

func (tg *Telegram) inlinePreview(illust *pixiv.Illust) {

	exists, err := tg.mc.BucketExists("pixiv")
	if err != nil || !exists {
		tg.l.Error().Int("illust", illust.ID).Err(err).Msg("missing pixiv bucket")
		return
	}

	oname := strconv.Itoa(illust.ID) + ".jpg"
	_, err = tg.mc.StatObject("pixiv", oname, minio.StatObjectOptions{})
	if err == nil {
		return // no need to upload if object is stat-able
	}

	rc, err := tg.pixiv.Download(illust, pixiv.SizeSquareMedium, 0)
	if err != nil {
		tg.l.Error().Int("illust", illust.ID).Err(err).Msg("cant download")
		return
	}
	defer rc.Close()

	_, err = tg.mc.PutObject("pixiv", oname, rc, -1, minio.PutObjectOptions{})
	if err != nil {
		tg.l.Error().Int("illust", illust.ID).Err(err).Msg("cant upload")
		return
	}
}
