package telegram

import (
	"bytes"
	"fmt"
	"math/rand"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/cking/pixiv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/patrickmn/go-cache"
	"gitlab.com/cking/pixy/models"
)

func (tg *Telegram) hSubscribe(inline bool) msgHandler {
	return func(msg tbm) error {
		u, err := tg.dbUser(msg.Chat.ID)
		if err != nil {
			return err
		}
		if err := tg.db.UpdateField(u, "Subscribed", true); err != nil {
			return err
		}

		if inline {
			return tg.edit(msg, "You have successfully subscribed to the image feed!", nil)
		}

		return tg.send(msg, `You have successfully subscribed to the image feed!
If you want to unsubscribe, use /unsubscribe.
Use /skip [n] to only display every n'th image,
and /quiet [from]-[to] to not send any message between [from] and [to] (24h format, UTC time).

Use http://www.timebie.com/std/utc.php to convert your local time (green) to UTC (blue).`)
	}
}

func (tg *Telegram) hUnsubscribe(inline bool) msgHandler {
	return func(msg tbm) error {
		u, err := tg.dbUser(msg.Chat.ID)
		if err != nil {
			return err
		}
		if err := tg.db.UpdateField(u, "Subscribed", false); err != nil {
			return err
		}

		if inline {
			return tg.edit(msg, "You have successfully unsubscribed from the image feed!", nil)
		}

		return tg.send(msg, `You have successfully unsubscribed from the image feed!
Sad to see you leave, feel free to /subscribe again any time`)
	}
}

func (tg *Telegram) hQuiet(inline bool) msgHandler {
	return func(msg tbm) error {
		if inline || (msg.CommandArguments() == "" && msg.Chat.IsPrivate()) {
			tg.cache.Set("tg:handler:"+strconv.FormatInt(msg.Chat.ID, 10), tg.haSkip, cache.DefaultExpiration)
			text := "Please give me the time frame to be quiet or use `x` or ❌ to disable quiet times. " +
				"The time has to be given in UTC, use something like http://www.timebie.com/std/utc.php to get UTC times!"
			kb := tgbotapi.NewReplyKeyboard(tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("❌"),
			))
			kb.OneTimeKeyboard = true
			kb.ResizeKeyboard = true

			return tg.sendComplex(msg, text, kb)
		}

		if msg.CommandArguments() == "" {
			tg.send(msg, "Please specify the time frame to be quiet, or use `x` or ❌ to disable quiet times!")
			return nil
		}

		err := tg.setQuiet(msg.Chat.ID, msg.CommandArguments())
		switch {
		case err == errUnparseable:
			tg.send(msg, "Invalid time frame format, use `hh:mm - hh:mm`. Please try again!")
		default:
			return err
		}

		return tg.sendComplex(msg, "Updated quiet times!", telegramBackKeyboard)
	}
}

func (tg *Telegram) hSkip(inline bool) msgHandler {
	return func(msg tbm) error {
		if inline || (msg.CommandArguments() == "" && msg.Chat.IsPrivate()) {
			tg.cache.Set("tg:handler:"+strconv.FormatInt(msg.Chat.ID, 10), tg.haSkip, cache.DefaultExpiration)
			text := "Please give me the amount of images to skip. There are 3 images per hour.\nSkipping 2 images displays 1 image per hour"
			kb := tgbotapi.NewReplyKeyboard(tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("0"),
				tgbotapi.NewKeyboardButton("2"),
			))
			kb.OneTimeKeyboard = true
			kb.ResizeKeyboard = true

			return tg.sendComplex(msg, text, kb)
		}

		if msg.CommandArguments() == "" {
			tg.send(msg, "Please specify the number of images to skip!")
			return nil
		}

		n, err := tg.setSkip(msg.Chat.ID, msg.CommandArguments())
		if err != nil {
			return err
		}

		nth := ""
		if n != 1 {
			nth = "s"
		}
		return tg.sendComplex(msg, fmt.Sprintf(
			"You will now skip %v image%v!",
			n, nth,
		), telegramBackKeyboard)
	}
}

func (tg *Telegram) haQuiet(msg tbm) error {
	err := tg.setQuiet(msg.Chat.ID, msg.Text)
	switch {
	case err == errUnparseable:
		kb := tgbotapi.NewReplyKeyboard(tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("❌"),
		))
		kb.OneTimeKeyboard = true
		kb.ResizeKeyboard = true
		tg.sendComplex(msg, "Invalid time frame format, use `hh:mm - hh:mm`. Please try again!", kb)
	default:
		return err
	}
	tg.cache.Delete("tg:handler:" + strconv.FormatInt(msg.Chat.ID, 10))

	return tg.sendComplex(msg, "Updated quiet times!", telegramBackKeyboard)
}

func (tg *Telegram) setQuiet(chid int64, tf string) error {
	u, err := tg.dbUser(chid)
	if err != nil {
		return err
	}

	if tf == "x" || tf == "❌" {
		return tg.db.UpdateField(u, "QuietEnabled", false)
	}

	times := strings.SplitN(tf, "-", 2)
	if len(times) != 2 {
		return errUnparseable
	}

	from, err := time.Parse("15:04", strings.TrimSpace(times[0]))
	if err != nil {
		return errUnparseable
	}

	to, err := time.Parse("15:04", strings.TrimSpace(times[0]))
	if err != nil {
		return errUnparseable
	}

	return tg.db.Update(&models.User{
		ID:           u.ID,
		QuietEnabled: true,
		QuietStart:   from,
		QuietEnd:     to,
	})
}

func (tg *Telegram) haSkip(msg tbm) error {
	tg.cache.Delete("tg:handler:" + strconv.FormatInt(msg.Chat.ID, 10))

	n, err := tg.setSkip(msg.Chat.ID, msg.Text)
	if err != nil {
		return err
	}

	nth := ""
	if n != 1 {
		nth = "s"
	}
	return tg.sendComplex(msg, fmt.Sprintf(
		"You will now skip %v image%v!",
		n, nth,
	), telegramBackKeyboard)
}

func (tg *Telegram) setSkip(chid int64, n string) (int, error) {
	u, err := tg.dbUser(chid)
	if err != nil {
		return 0, err
	}

	ni, err := strconv.Atoi(n)
	if err != nil {
		return 0, err
	}

	return ni, tg.db.UpdateField(u, "Skip", ni)
}

func (tg *Telegram) hSearch(inline bool) msgHandler {
	return func(msg tbm) error {
		if inline || (msg.CommandArguments() == "" && msg.Chat.IsPrivate()) {
			tg.cache.Set("tg:handler:"+strconv.FormatInt(msg.Chat.ID, 10), tg.haSearch, cache.DefaultExpiration)
			text := "Please give me the search terms to look for"
			return tg.sendComplex(msg, text, nil)
		}

		if msg.CommandArguments() == "" {
			tg.send(msg, "Please specify the search terms!")
			return nil
		}

		tg.sendComplex(msg, "Querying pixiv...", nil)
		fb, err := tg.search(msg.CommandArguments())
		if err != nil {
			return err
		}

		if fb == nil {
			return tg.sendComplex(msg, "No image found!", telegramBackKeyboard)
		}

		return tg.sendPhoto(msg, fb, nil)
	}
}

func (tg *Telegram) haSearch(msg tbm) error {
	tg.cache.Delete("tg:handler:" + strconv.FormatInt(msg.Chat.ID, 10))

	tg.sendComplex(msg, "Querying pixiv...", nil)
	fb, err := tg.search(msg.Text)
	if err != nil {
		return err
	}

	if fb == nil {
		return tg.sendComplex(msg, "No image found!", telegramBackKeyboard)
	}

	return tg.sendPhoto(msg, fb, nil)
}

func (tg *Telegram) search(terms string) (*tgbotapi.FileBytes, error) {
	tg.l.Debug().Str("terms", terms).Msg("starting search...")
	illusts, err := tg.pixiv.IllustSearch(terms, pixiv.SearchPartialTags, 1)
	is := illusts.Illusts
	if err != nil {
		return nil, err
	}
	tg.l.Debug().Str("terms", terms).Int("results", len(is)).Msg("found images!")

	if len(is) == 0 {
		return nil, nil
	}

	n := rand.Intn(len(is))
	tg.l.Debug().Str("terms", terms).Int("result", n).Msgf("using %v", is[n].ID)
	rc, err := tg.pixiv.Download(&is[n], pixiv.SizeMedium, 0)
	if err != nil {
		return nil, err
	}
	defer rc.Close()

	b := &bytes.Buffer{}
	_, err = b.ReadFrom(rc)
	if err != nil {
		return nil, err
	}

	fname := tg.pixiv.DownloadLink(&is[n], pixiv.SizeMedium, 0)
	fname = filepath.Base(fname)

	fb := new(tgbotapi.FileBytes)
	fb.Name = fname
	fb.Bytes = b.Bytes()
	return fb, nil
}
