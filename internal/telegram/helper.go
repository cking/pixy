package telegram

import (
	"strconv"

	"github.com/asdine/storm"

	"github.com/asdine/storm/q"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/cking/pixy/models"
)

var (
	telegramDefaultKeyboard = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Subscribe", "subscribe"),
			tgbotapi.NewInlineKeyboardButtonData("Search", "search"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Settings", "settings"),
		),
	)

	telegramSubscribedKeyboard = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Unsubscribe", "unsubscribe"),
			tgbotapi.NewInlineKeyboardButtonData("Search", "search"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Settings", "settings"),
		),
	)

	telegramSettingsKeyboard = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Set quiet times", "quiet"),
			tgbotapi.NewInlineKeyboardButtonData("Skip images", "skip"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("« Back to the main menu", "start"),
		),
	)

	telegramBackKeyboard = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("« Back to the main menu", "start"),
		),
	)
)

func (tg *Telegram) dbUser(chatID int64) (*models.User, error) {
	chid := strconv.FormatInt(chatID, 10)
	u := new(models.User)
	switch err := tg.db.Select(
		q.And(
			q.Eq("Source", "telegram"),
			q.Eq("SourceID", chid),
		),
	).First(u); err {
	case nil:
		// do nothing
	case storm.ErrNotFound:
		u.Source = "telegram"
		u.SourceID = chid
		if err := tg.db.Save(u); err != nil {
			return nil, err
		}
	default:
		return nil, err
	}

	return u, nil
}

func (tg *Telegram) isSubscribed(chID int64) bool {
	if u, err := tg.dbUser(chID); err != nil || !u.Subscribed {
		return false
	}
	return true
}

func (tg *Telegram) send(msg tbm, m string) error {
	var rm interface{}
	if tg.isSubscribed(msg.Chat.ID) {
		rm = telegramDefaultKeyboard
	} else {
		rm = telegramSubscribedKeyboard
	}
	return tg.sendComplex(msg, m, rm)
}

func (tg *Telegram) sendComplex(msg tbm, m string, reply interface{}) error {
	mc := tgbotapi.NewMessage(msg.Chat.ID, m)
	mc.ParseMode = tgbotapi.ModeMarkdown

	if msg.Chat.IsPrivate() {
		if reply != nil {
			mc.ReplyMarkup = reply
		}
	}

	_, err := tg.bot.Send(mc)
	return err
}

func (tg *Telegram) sendPhoto(msg tbm, fr *tgbotapi.FileBytes, reply interface{}) error {
	mc := tgbotapi.NewPhotoUpload(msg.Chat.ID, *fr)

	if msg.Chat.IsPrivate() {
		if reply != nil {
			mc.ReplyMarkup = reply
		} else if tg.isSubscribed(msg.Chat.ID) {
			mc.ReplyMarkup = telegramDefaultKeyboard
		} else {
			mc.ReplyMarkup = telegramSubscribedKeyboard
		}
	}

	_, err := tg.bot.Send(mc)
	return err
}

func (tg *Telegram) edit(msg tbm, m string, customKeyboard *tgbotapi.InlineKeyboardMarkup) error {
	mc := tgbotapi.NewEditMessageText(msg.Chat.ID, msg.MessageID, m)
	mc.ParseMode = tgbotapi.ModeMarkdown
	_, err := tg.bot.Send(mc)
	if err != nil || !msg.Chat.IsPrivate() {
		return err
	}

	if customKeyboard == nil {
		customKeyboard = &telegramBackKeyboard
	}
	_, err = tg.bot.Send(tgbotapi.NewEditMessageReplyMarkup(msg.Chat.ID, msg.MessageID, *customKeyboard))
	return err
}
