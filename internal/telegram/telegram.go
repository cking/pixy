package telegram

import (
	"bytes"
	"strconv"
	"time"

	"github.com/asdine/storm"
	"github.com/asdine/storm/q"
	"github.com/cking/pixiv"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/minio/minio-go"
	"github.com/patrickmn/go-cache"
	"github.com/rs/zerolog"
	"gitlab.com/cking/pixy/models"
	"gitlab.com/cking/pixy/services"
	"golang.org/x/xerrors"
)

// Telegram is the telegram bot api connector
type Telegram struct {
	bot   *tgbotapi.BotAPI
	l     *zerolog.Logger
	cache *cache.Cache
	db    *storm.DB
	pixiv *pixiv.Session
	mc    *minio.Client

	breakLoop chan struct{}

	commands  map[string]msgHandler
	callbacks map[string]msgHandler
}

// Configure fetches objects from the container and configures application specific logic
func (tg *Telegram) Configure(c *services.Container) error {
	var err error
	if tg.bot, err = c.GetTelegram(); err != nil {
		return xerrors.Errorf("failed to fetch telegram: %w", err)
	}

	if tg.db, err = c.GetBolt(); err != nil {
		return xerrors.Errorf("failed to fetch db: %w", err)
	}

	if tg.pixiv, err = c.GetPixiv(); err != nil {
		return xerrors.Errorf("failed to fetch pixiv: %w", err)
	}

	if tg.mc, err = c.GetMinio(); err != nil {
		return xerrors.Errorf("failed to fetch minio: %w", err)
	}

	if tg.l, err = c.GetLogger(); err != nil {
		return xerrors.Errorf("failed to fetch logger: %w", err)
	}
	l := tg.l.With().Str("module", "telegram").Logger()
	tg.l = &l

	tg.cache = cache.New(time.Minute, 10*time.Minute)
	tg.installMessageHandlers()
	tg.installCallbackHandlers()

	return nil
}

// Run launches this pixkin
func (tg *Telegram) Run(c *services.Container) error {
	tg.l.Info().Str("username", tg.bot.Self.String()).Msg("authenticated!")
	tg.l.Debug().Msg("starting update channel")

	uc, err := tg.bot.GetUpdatesChan(tgbotapi.UpdateConfig{})
	if err != nil {
		tg.l.Error().Err(err).Msg("failed to initialize update channel")
	}

	for {
		select {
		case <-tg.breakLoop:
			tg.l.Info().Msg("Stopping!")
			tg.bot.StopReceivingUpdates()
			return nil

		case u := <-uc:
			switch {
			case u.CallbackQuery != nil:
				err := tg.executeCallback(u.CallbackQuery)
				if err != nil {
					tg.l.Error().Err(err).Msg("callback query errored")
				}

			case u.InlineQuery != nil:
				err := tg.executeInline(u.InlineQuery)
				if err != nil {
					tg.l.Error().Err(err).Str("query", u.InlineQuery.Query).Msg("inline query errored")
				}

			default:
				err := tg.executeMessage(&u)
				if err != nil {
					tg.l.Error().Err(err).Msg("message errored")
				}
			}
		}
	}
}

// Break the main loop
func (tg *Telegram) Break() {
	close(tg.breakLoop)
}

// Post the specified image to all subscribers
func (tg *Telegram) Post(filename string, buffer *bytes.Buffer, illust *pixiv.Illust) {
	tg.l.Info().Msg("Starting post schedule")
	now := time.Now().UTC()

	var fileID string
	var users []*models.User
	err := tg.db.Select(q.And(q.Eq("Source", "telegram"), q.Eq("Subscribed", true))).
		Find(&users)
	if err != nil {
		tg.l.Error().Err(err).Msg("failed to fetch users")
		return
	}
	tg.l.Debug().Int("users", len(users)).Msg("possible users")

	for _, u := range users {
		uid, _ := strconv.Atoi(u.SourceID)
		tg.l.Debug().Int("user_id", uid).Msg("checking user")

		tg.l.Debug().Int("user_id", uid).Bool("quiet", u.QuietEnabled).Time("from", u.QuietStart).Time("to", u.QuietEnd).Msg("quiet hours")
		if u.QuietEnabled {
			if u.QuietStart.Before(u.QuietEnd) && u.QuietStart.Before(now) && u.QuietEnd.After(now) {
				continue
			} else if u.QuietStart.Before(now) || u.QuietEnd.After(now) {
				continue
			}
		}

		tg.l.Debug().Int("user_id", uid).Int("skip", u.Skip).Msg("image skipping")
		if u.Skip > 0 {
			skip := 1
			rskip, ok := tg.cache.Get("telegram:skip:" + u.SourceID)
			if ok {
				skip = rskip.(int) + 1
			}

			if skip > u.Skip {
				skip = 0
			}

			tg.cache.Set("telegram:skip:"+u.SourceID, skip, cache.NoExpiration)

			if skip > 0 {
				continue
			}
		}

		tg.l.Debug().Int("user_id", uid).Msg("posting image")
		var c tgbotapi.Chattable
		chatID, _ := strconv.ParseInt(u.ChatID, 10, 64)

		if fileID != "" {
			c = tgbotapi.NewPhotoShare(chatID, fileID)
		} else {
			c = tgbotapi.NewPhotoUpload(chatID, tgbotapi.FileBytes{
				Name:  filename,
				Bytes: buffer.Bytes(),
			})
		}
		m, err := tg.bot.Send(c)
		if err != nil {
			tg.l.Error().Err(err).Int("user_id", uid).Int64("chat_id", chatID).Msg("failed to post image")
		} else if fileID == "" {
			photos := *m.Photo
			fileID = photos[0].FileID
		}
	}
}
