package telegram

import (
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type tbm = *tgbotapi.Message
type msgHandler = func(m tbm) error
type msgHandlerEx = func(edit bool) msgHandler

func (tg *Telegram) installMessageHandlers() {
	tg.commands = map[string]msgHandler{
		"start":       tg.mStart,
		"subscribe":   tg.hSubscribe(false),
		"unsubscribe": tg.hUnsubscribe(false),
		"quiet":       tg.hQuiet(false),
		"skip":        tg.hSkip(false),
		"search":      tg.hSearch(false),
	}
}

func (tg *Telegram) mStart(m tbm) error {
	_, err := tg.dbUser(m.Chat.ID)
	if err != nil {
		return err
	}

	return tg.send(m, "Hello "+m.From.FirstName+`,

use the buttons below to have some fun.
Feel free to invite me to your group chats!
	`)
}

func (tg *Telegram) executeMessage(u *tgbotapi.Update) error {
	l := tg.l.With().Int64("chat_id", u.Message.Chat.ID).Int("message_id", u.Message.MessageID).Logger()
	l.Debug().
		Str("message", u.Message.Text).
		Msg("message query")

	var handler msgHandler
	if iHandler, found := tg.cache.Get("tg:handler:" + strconv.FormatInt(u.Message.Chat.ID, 10)); found {
		handler = iHandler.(msgHandler)
		l.Debug().Msg("found old handler on cache!")
	} else if !u.Message.IsCommand() {
		l.Debug().Int64("chat_id", u.Message.Chat.ID).Msg("ignoring regular message")
		return nil
	} else if handler, found = tg.commands[u.Message.Command()]; found {
		l.Debug().Str("command", u.Message.Command()).Msg("command found, executing!")
	} else {
		l.Warn().Str("command", u.Message.Command()).Msg("unkown command found, skipping!")
		return nil
	}

	return handler(u.Message)
}
