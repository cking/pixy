// +build mage

package main

import (
	"io/ioutil"
	"path/filepath"
	"runtime"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/target"
	"gitlab.com/bakyun/magician"
)

var (
	isWin   = runtime.GOOS == "windows"
	Default = Build

	buildTags = ""
)

func Build() error {
	magician.Headline("Build")

	exe := "out/debug"
	if isWin {
		exe += ".exe"
	}

	magician.Exec("go", "generate", "./...")

	files, err := ioutil.ReadDir(".")
	var paths []string
	for _, f := range files {
		if f.IsDir() && (f.Name() == "out" || f.Name()[0] == byte('.')) {
			continue
		}

		if !f.IsDir() && filepath.Ext(f.Name()) != ".go" {
			continue
		}

		paths = append(paths, f.Name())
	}

	build, err := target.Dir(exe, paths...)
	if err != nil {
		return err
	}

	if !build {
		magician.Echo("skipping, already up to date!")
		return nil
	}

	args := []string{
		"build", "-tags", buildTags, "-race",
		"-o", exe,
		"-ldflags", magician.GitVersionLDFlags(magician.GetGoPackageName("./config")),
	}

	if mg.Verbose() {
		args = append(args, "-v")
	}

	args = append(args, "./cmd/pixy")

	return magician.Exec("go", args...)
}

func Dev() {
	magician.Headline("Configure")
	magician.Echo("Enabling development environment")
	buildTags = "dev"
	mg.SerialDeps(Build)
}

func Changelog() error {
	magician.Headline("Generate CHANGELOG.md")
	return magician.Exec("go", "run", "github.com/git-chglog/git-chglog/cmd/git-chglog", "-o", "CHANGELOG.md")
}
