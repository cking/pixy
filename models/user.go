package models

import "time"

// User stores user metadata
type User struct {
	ID           int    `storm:"id,increment"`
	Source       string `storm:"index"`
	SourceID     string `storm:"index"`
	ChatID       string
	Skip         int
	QuietStart   time.Time
	QuietEnd     time.Time
	Subscribed   bool
	QuietEnabled bool
}
