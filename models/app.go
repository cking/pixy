package models

// App stores oauth app info
type App struct {
	Server       string `storm:"id"`
	ClientID     string
	ClientSecret string
}
