package main

import (
	"gitlab.com/cking/pixy/internal/pixkin"

	"gitlab.com/cking/pixy/services"
)

func failIf(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	failIf(services.DefaultContainer.Run(new(pixkin.Pixkin)))
}
