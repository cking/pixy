package config

import (
	"os"

	"github.com/octago/sflags/gen/gpflag"
	"github.com/spf13/pflag"
	"golang.org/x/xerrors"

	"github.com/jinzhu/configor"
)

// Configuration stores the runtime application settings
type Configuration struct {
	/*
		struct tags:
			env: custom environment variable naming
			default: default value of field
			required[true, false]: make field required, default false
	*/

	Pixiv struct {
		Username     string  `required:"true" json:"username" yaml:"username" toml:"username"`
		Password     string  `required:"true" json:"password" yaml:"password" toml:"password"`
		ClientID     string  `required:"true" json:"client_id" yaml:"client_id" toml:"client_id"`
		ClientSecret string  `required:"true" json:"client_secret" yaml:"client_secret" toml:"client_secret"`
		FavChance    float64 `default:"5" json:"fav_chance" yaml:"fav_chance" toml:"fav_chance"`
	} `json:"pixiv" yaml:"pixiv" toml:"pixiv"`

	Telegram struct {
		Enabled bool   `default:"false" json:"enabled" yaml:"enabled" toml:"enabled"`
		Token   string `json:"token" yaml:"token" toml:"token"`
	} `json:"telegram" yaml:"telegram" toml:"telegram"`

	Mastodon struct {
		Enabled  bool   `default:"false" json:"enabled" yaml:"enabled" toml:"enabled"`
		Instance string `json:"instance" yaml:"instance" toml:"instance"`
		Username string `required:"true" json:"username" yaml:"username" toml:"username"`
		Password string `required:"true" json:"password" yaml:"password" toml:"password"`
	} `json:"mastodon" yaml:"mastodon" toml:"mastodon"`

	Bolt struct {
		Path string `default:"bunt.db" json:"path" yaml:"path" toml:"path"`
	} `json:"bunt" yaml:"bunt" toml:"bunt"`

	S3 struct {
		Endpoint  string `json:"endpoint" yaml:"endpoint" toml:"endpoint"`
		AccessKey string `json:"access_key" yaml:"access_key" toml:"access_key"`
		SecretKey string `json:"secret_key" yaml:"secret_key" toml:"secret_key"`
		Secure    bool   `json:"secure" yaml:"secure" toml:"secure"`
	} `json:"s3" yaml:"s3" toml:"s3"`
}

// Load configuration from config files
func (cfg *Configuration) Load(customFile string) error {
	env := "development"
	if !Dev {
		env = "production"
	}

	var configFiles []string
	if customFile != "" {
		configFiles = []string{customFile}
	}
	configFiles = append(configFiles, "config.yml", "config.yaml", ApplicationName+".yml", ApplicationName+".yaml")

	err := configor.New(&configor.Config{
		Environment:          env,
		ENVPrefix:            ApplicationName,
		ErrorOnUnmatchedKeys: true,
	}).Load(cfg, configFiles...)

	if err != nil {
		return xerrors.Errorf("failed to parse config file: %w", err)
	}
	return nil
}

// LoadAndFlags loads config file and command line flags for the configuration
func (cfg *Configuration) LoadAndFlags() error {
	configFile := pflag.StringP("config", "c", "", "Custom configuration file to load")
	pflag.Parse()

	if err := cfg.Load(*configFile); err != nil {
		return err
	}

	fs, err := gpflag.Parse(cfg)
	if err != nil {
		return xerrors.Errorf("failed to parse flag structure: %w", err)
	}
	fs.StringP("config", "c", "", "Custom configuration file to load")

	fs.Parse(os.Args[1:])
	return nil
}
