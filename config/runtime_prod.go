// +build !dev

package config

const (
	// Dev defines if the application is built in development mode
	Dev = true
)
