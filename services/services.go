package services

import (
	"github.com/asdine/storm"
	"github.com/cking/pixiv"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/mattn/go-mastodon"
	"github.com/minio/minio-go"
	"github.com/rs/zerolog"
	"gitlab.com/cking/pixy/config"
)

// DefaultContainer is the default container
var DefaultContainer = new(Container)

// Runnable defines a runnable struct
type Runnable interface {
	Run(*Container) error
}

// Configurable defines a configureable struct
type Configurable interface {
	Configure(*Container) error
}

// Container stores all providers
type Container struct {
	config   *config.Configuration
	pixiv    *pixiv.Session
	telegram *tgbotapi.BotAPI
	logger   *zerolog.Logger
	bolt     *storm.DB
	minio    *minio.Client
	mastodon *mastodon.Client
}

// Run configures and launches the given Runnable
func (c *Container) Run(r Runnable) error {
	if rc, ok := r.(Configurable); ok {
		if err := rc.Configure(c); err != nil {
			return err
		}
	}

	return r.Run(c)
}
