package services

import (
	"io"
	stdlog "log"
	"os"
	"path"
	"runtime"
	"strconv"
	"time"

	"github.com/mattn/go-isatty"
	"github.com/roger-king/color"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/cking/pixy/config"
)

// Logger returns the current Logger or panics
func (c *Container) Logger() *zerolog.Logger {
	cfg, err := c.GetLogger()
	failIf(err)
	return cfg
}

// GetLogger returns the current Logger or an error
func (c *Container) GetLogger() (*zerolog.Logger, error) {
	if c.logger == nil {
		cfg, err := c.BuildLogger()
		if err != nil {
			return nil, err
		}
		c.logger = cfg
	}

	return c.logger, nil
}

// BuildLogger builds the default Logger object
func (c *Container) BuildLogger() (*zerolog.Logger, error) {
	if !config.Dev {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	}

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if config.Dev {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	var writer io.Writer = color.Output

	if isatty.IsTerminal(os.Stdout.Fd()) {
		writer = zerolog.NewConsoleWriter(func(cw *zerolog.ConsoleWriter) {
			cw.Out = writer
			cw.TimeFormat = time.RFC3339
		})
	}

	_, basepath, _, _ := runtime.Caller(1)
	basepath = path.Join(basepath, "..", "..")

	zerolog.CallerMarshalFunc = func(file string, line int) string {
		return file[len(basepath):] + ":" + strconv.Itoa(line)
	}

	logger := zerolog.New(writer).With().
		Timestamp().
		Caller().
		Stack().
		Logger()
	log.Logger = logger
	stdlog.SetFlags(0)
	stdlog.SetOutput(logger)

	return &logger, nil
}
