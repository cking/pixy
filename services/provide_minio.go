package services

import (
	"github.com/minio/minio-go"
	"golang.org/x/xerrors"
)

// Minio returns the current Minio or panics
func (c *Container) Minio() *minio.Client {
	cfg, err := c.GetMinio()
	failIf(err)
	return cfg
}

// GetMinio returns the current Minio or an error
func (c *Container) GetMinio() (*minio.Client, error) {
	if c.minio == nil {
		o, err := c.BuildMinio()
		if err != nil {
			return nil, err
		}
		c.minio = o
	}

	return c.minio, nil
}

// BuildMinio builds the default Minio object
func (c *Container) BuildMinio() (*minio.Client, error) {
	cfg, err := c.GetConfig()
	if err != nil {
		return nil, xerrors.Errorf("failed to build Minio: %w", err)
	}

	mc, err := minio.New(cfg.S3.Endpoint, cfg.S3.AccessKey, cfg.S3.SecretKey, cfg.S3.Secure)
	if err != nil {
		return nil, xerrors.Errorf("failed to build Minio: %w", err)
	}

	return mc, nil
}
