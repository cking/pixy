package services

import (
	"github.com/cking/pixiv"
	"golang.org/x/xerrors"
)

// Pixiv returns the current pixiv instance or panics
func (c *Container) Pixiv() *pixiv.Session {
	o, err := c.GetPixiv()
	failIf(err)
	return o
}

// GetPixiv returns the current pixiv instance or an error
func (c *Container) GetPixiv() (*pixiv.Session, error) {
	if c.pixiv == nil {
		o, err := c.BuildPixiv()
		if err != nil {
			return nil, err
		}
		c.pixiv = o
	}

	return c.pixiv, nil
}

// BuildPixiv builds the default pixiv instance
func (c *Container) BuildPixiv() (*pixiv.Session, error) {
	cfg, err := c.GetConfig()
	if err != nil {
		return nil, xerrors.Errorf("failed to build pixiv: %w", err)
	}

	s := pixiv.NewSession()
	_, err = s.Login(cfg.Pixiv.Username, cfg.Pixiv.Password)
	if err != nil {
		return nil, xerrors.Errorf("failed to build pixiv: %w", err)
	}

	return s, nil
}
