package services

func failIf(err error) {
	if err != nil {
		panic(err)
	}
}
