package services

import (
	"gitlab.com/cking/pixy/config"
	"golang.org/x/xerrors"
)

// Config returns the current config or panics
func (c *Container) Config() *config.Configuration {
	cfg, err := c.GetConfig()
	failIf(err)
	return cfg
}

// GetConfig returns the current config or an error
func (c *Container) GetConfig() (*config.Configuration, error) {
	if c.config == nil {
		cfg, err := c.BuildConfig()
		if err != nil {
			return nil, err
		}
		c.config = cfg
	}

	return c.config, nil
}

// BuildConfig builds the default config object
func (c *Container) BuildConfig() (*config.Configuration, error) {
	cfg := new(config.Configuration)
	if err := cfg.LoadAndFlags(); err != nil {
		return nil, xerrors.Errorf("failed to build config: %w", err)
	}

	return cfg, nil
}
