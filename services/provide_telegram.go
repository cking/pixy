package services

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"golang.org/x/xerrors"
)

// Telegram returns the current Telegram instance or panics
func (c *Container) Telegram() *tgbotapi.BotAPI {
	o, err := c.GetTelegram()
	failIf(err)
	return o
}

// GetTelegram returns the current Telegram instance or an error
func (c *Container) GetTelegram() (*tgbotapi.BotAPI, error) {
	if c.telegram == nil {
		o, err := c.BuildTelegram()
		if err != nil {
			return nil, err
		}
		c.telegram = o
	}

	return c.telegram, nil
}

// BuildTelegram builds the default Telegram instance
func (c *Container) BuildTelegram() (*tgbotapi.BotAPI, error) {
	cfg, err := c.GetConfig()
	if err != nil {
		return nil, xerrors.Errorf("failed to build Telegram: %w", err)
	}

	api, err := tgbotapi.NewBotAPI(cfg.Telegram.Token)
	if err != nil {
		return nil, xerrors.Errorf("failed to build Telegram: %w", err)
	}

	return api, nil
}
