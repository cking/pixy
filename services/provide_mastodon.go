package services

import (
	"context"

	"gitlab.com/cking/pixy/config"

	"github.com/asdine/storm"
	"github.com/mattn/go-mastodon"
	"gitlab.com/cking/pixy/models"
	"golang.org/x/xerrors"
)

// Mastodon returns the current Mastodon or panics
func (c *Container) Mastodon() *mastodon.Client {
	cfg, err := c.GetMastodon()
	failIf(err)
	return cfg
}

// GetMastodon returns the current Mastodon or an error
func (c *Container) GetMastodon() (*mastodon.Client, error) {
	if c.mastodon == nil {
		o, err := c.BuildMastodon()
		if err != nil {
			return nil, err
		}
		c.mastodon = o
	}

	return c.mastodon, nil
}

// BuildMastodon builds the default Mastodon object
func (c *Container) BuildMastodon() (*mastodon.Client, error) {
	cfg, err := c.GetConfig()
	if err != nil {
		return nil, xerrors.Errorf("failed to build Mastodon: %w", err)
	}

	db, err := c.GetBolt()
	if err != nil {
		return nil, xerrors.Errorf("failed to build Mastodon: %w", err)
	}

	app := new(models.App)
	switch err := db.One("Server", cfg.Mastodon.Instance, app); err {
	case nil:
		// do nothing
	case storm.ErrNotFound:
		app = new(models.App)
		app.Server = cfg.Mastodon.Instance

		mapp, err := mastodon.RegisterApp(context.Background(), &mastodon.AppConfig{
			Server:       app.Server,
			ClientName:   config.ApplicationName,
			RedirectURIs: "urn:ietf:wg:oauth:2.0:oob",
			Scopes:       "read write follow",
		})
		if err != nil {
			return nil, xerrors.Errorf("failed to build Mastodon: %w", err)
		}

		app.ClientID = mapp.ClientID
		app.ClientSecret = mapp.ClientSecret
		if err := db.Save(app); err != nil {
			return nil, xerrors.Errorf("failed to build Mastodon: %w", err)
		}
	default:
		return nil, xerrors.Errorf("failed to build Mastodon: %w", err)
	}

	client := mastodon.NewClient(&mastodon.Config{
		Server:       app.Server,
		ClientID:     app.ClientID,
		ClientSecret: app.ClientSecret,
	})

	if err := client.Authenticate(context.Background(), cfg.Mastodon.Username, cfg.Mastodon.Password); err != nil {
		return nil, xerrors.Errorf("failed to build Mastodon: %w", err)
	}

	return client, nil
}
