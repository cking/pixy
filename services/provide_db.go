package services

import (
	"github.com/asdine/storm"
	"github.com/asdine/storm/codec/gob"
	"golang.org/x/xerrors"
)

// Bolt returns the current Bolt or panics
func (c *Container) Bolt() *storm.DB {
	cfg, err := c.GetBolt()
	failIf(err)
	return cfg
}

// GetBolt returns the current Bolt or an error
func (c *Container) GetBolt() (*storm.DB, error) {
	if c.bolt == nil {
		o, err := c.BuildBolt()
		if err != nil {
			return nil, err
		}
		c.bolt = o
	}

	return c.bolt, nil
}

// BuildBolt builds the default Bolt object
func (c *Container) BuildBolt() (*storm.DB, error) {
	cfg, err := c.GetConfig()
	if err != nil {
		return nil, xerrors.Errorf("failed to build Bolt: %w", err)
	}

	db, err := storm.Open(cfg.Bolt.Path, storm.Codec(gob.Codec))
	if err != nil {
		return nil, xerrors.Errorf("failed to build Bolt: %w", err)
	}

	return db, nil
}
