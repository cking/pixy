module gitlab.com/cking/pixy

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/DataDog/zstd v1.4.0 // indirect
	github.com/Sereal/Sereal v0.0.0-20190606082811-cf1bab6c7a3a // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/cking/pixiv v0.0.0-20190903121104-bd8f37b0dfc0
	github.com/go-ini/ini v1.42.0 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/golang/snappy v0.0.1 // indirect
	github.com/jinzhu/configor v1.0.0
	github.com/kanosaki/gopixiv2 v0.0.0-20181112163639-5a66b61968dc
	github.com/kr/pretty v0.1.0 // indirect
	github.com/magefile/mage v1.8.0
	github.com/mattn/go-isatty v0.0.8
	github.com/mattn/go-mastodon v0.0.4
	github.com/minio/minio-go v6.0.14+incompatible
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/octago/sflags v0.2.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/robfig/cron v1.1.0
	github.com/roger-king/color v1.7.0
	github.com/rs/zerolog v1.14.3
	github.com/sirupsen/logrus v1.4.1 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190306220146-200a235640ff // indirect
	github.com/spf13/pflag v1.0.3
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	gitlab.com/bakyun/magician v0.0.0-20190210101925-cf313cf20e1e
	go.etcd.io/bbolt v1.3.3 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/ini.v1 v1.42.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
